const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs').promises;

const isNameValid = (name) => {
    const extensions = ['log', 'xml', 'json', 'js', 'yaml', 'txt'];
    const extension = name.split('.').slice(-1)[0];

    return extensions.some(item => extension === item)
}

app.use(bodyParser.json());

(async () => {
    const dirContent = await fs.readdir('./');
    if (!dirContent.some(item => item === 'files')) {
        await fs.mkdir('./files')
    }
})()

app.post('/api/files', async (req, res) => {
    console.log(req.body);

    const filename = req.body.filename;
    const content = req.body.content;

    if (!isNameValid(filename)) {
        res.status(400).json({ message: 'Please, use one of next file extensions: xml, yaml, json, js, txt, log'})
        return
    }

    if (!content) {
        res.status(400).json({ message: 'Please, specify "content" parameter'})
        return
    }

    try {
    const newFile = await  fs.writeFile('./files/' + filename, content)

    res.status(200).json({ message: 'File created successfully'})
    console.log(req.body)
    
    } catch (e) {
        res.status(500).json({ message: 'Server error'})
        console.log(e)
    }

})


app.get('/api/files/:fileName', async (req, res) => {
    try {
        console.log(req.params);

        const files = await fs.readdir('./files');

        if (!files.some(item => item === req.params.fileName)) {
            res.status(400).json({ message: `No file with ${req.params.fileName} filename found`});
            return;
        }

        const responseObject = {
            message: "Success",
            filename: req.params.fileName
        }

        const stat = await fs.stat('./files/' + req.params.fileName)


        responseObject.content = await fs.readFile('./files/' + req.params.fileName, {encoding: 'utf-8'});
        responseObject.extension = req.params.fileName.split('.').slice(-1)[0]
        responseObject.uploadedDate = stat.birthtime;

        res.status(200).json(responseObject)
        
    } catch (e) {
        console.log(e)
        res.status(500).json({ message: 'Server error'})
    }
})

app.get('/api/files', async (req, res) => {
    try{
    console.log(req.body);
    const files = await fs.readdir('./files');
    res.status(200).json({ message: "Success", files})
    } catch (e) {
        console.log(e);
        res.status(500).json({ message: "Server error" })
    }
})


app.listen(8080);